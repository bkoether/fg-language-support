<div class="wrap">
  <h2>FactorsGroup Language Tunnel</h2>
  <?php if ( $_GET['settings-updated'] ): ?>
    <div class="updated"><p><strong>Settings saved.</strong></p></div>
  <?php endif; ?>

  <form method="post" action="options.php">
    <?php settings_fields( 'fg-lang-settings-general' ); ?>
    <?php do_settings_sections( 'fg-lang-general' ); ?>
    <?php submit_button(); ?>
  </form>
</div>
