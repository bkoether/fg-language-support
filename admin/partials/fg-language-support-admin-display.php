<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://factorsgroup.com
 * @since      1.0.0
 *
 * @package    Fg_Language_Support
 * @subpackage Fg_Language_Support/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
