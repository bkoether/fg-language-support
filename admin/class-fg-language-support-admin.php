<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Fg_Language_Support
 * @subpackage Fg_Language_Support/admin
 * @author     Ben Koether <bkoether@factorsgroup.com>
 */
class Fg_Language_Support_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Fg_Language_Support_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Fg_Language_Support_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/fg-language-support-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Fg_Language_Support_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Fg_Language_Support_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/fg-language-support-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function adjust_admin_menu(WP_Admin_Bar $admin_bar) {
	  if (is_super_admin()) {
      $blogs = $admin_bar->user->blogs;
      foreach ($blogs as $id => $blog) {
        $admin_bar->user->blogs[$id]->blogname = $admin_bar->user->blogs[$id]->path;
      }
    }
  }

  public function add_language_settings() {
    $options = $this->lang_options();

    add_settings_section(
      'names', // ID
      'Site Settings', // Title
      array( $this, 'print_section_info' ), // Callback
      'fg-lang-general' // Page
    );
    foreach ($options as $id => $path) {
      add_settings_field(
        'fg_lang_site_name_' . $id,
        $path,
        array($this, 'site_name_field'),
        'fg-lang-general',
        'names',
        $id
      );
      register_setting( 'fg-lang-settings-general', 'fg_lang_site_name_' . $id );
      register_setting( 'fg-lang-settings-general', 'fg_lang_site_weight_' . $id );
      register_setting( 'fg-lang-settings-general', 'fg_lang_site_hidden_' . $id );
    }

    add_settings_section(
      'mapping', // ID
      'Country Mapping', // Title
      array( $this, 'print_section_info' ), // Callback
      'fg-lang-general' // Page
    );
    foreach ($options as $id => $path) {
      add_settings_field(
        'fg_lang_country_map_' . $id,
        $path,
        array($this, 'country_map_field'),
        'fg-lang-general',
        'mapping',
        $id
      );
      register_setting( 'fg-lang-settings-general', 'fg_lang_country_map_' . $id );
    }
    add_settings_field(
      'fg_lang_country_map_fallback',
      'Fallback Site',
      array($this, 'fallback_field'),
      'fg-lang-general',
      'mapping',
      $options
    );
    register_setting( 'fg-lang-settings-general', 'fg_lang_country_map_fallback');





  }

  public function add_settings_menu() {
	  $bid = get_current_blog_id();
	  if ($bid == 1) {
      add_menu_page('Language Tunnel', 'Language Tunnel', 'list_users',
        'fg-lang-general', [
          $this,
          'options_router',
        ], '', 3);
      add_submenu_page('fg-lang-general', 'General', 'General',
        'list_users', 'fg-lang-general');
    }
  }

  public function options_router() {
    $path = plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/';
    $page = $_GET['page'];

    switch ( $page ) {
      default:
        require_once( $path . 'settings-general.php' );
        break;
    }
  }

  public function site_name_field($bid) {
    $default = get_option('fg_lang_site_name_' . $bid, '');
    $out = 'Name: <input class="regular-text" type="text" name="fg_lang_site_name_' . $bid .'" id="fg_lang_site_name_"' . $bid . ' value="' . $default . '" />';

    $weight_default = get_option('fg_lang_site_weight_' . $bid, 0);
    $out .= '<br/>Weight: <input name="fg_lang_site_weight_' . $bid .'" id="fg_lang_site_weight_' . $bid . '" value="' . $weight_default . '" size="2" type="number" />';

    $hidden_default = get_option('fg_lang_site_hidden_' . $bid, 0);
    $out .= '<br/><label for="fg_lang_site_hidden_' . $bid . '"><input name="fg_lang_site_hidden_' . $bid .'" id="fg_lang_site_hidden_' . $bid . '" value="1" ' . ($hidden_default ? 'checked' : '') . ' type="checkbox"> Hide on language switcher page.</label>';
	  echo $out;
  }

  public function country_map_field($bid) {
    $default = get_option('fg_lang_country_map_' . $bid, '');
    $out = '<input class="regular-text" type="text" name="fg_lang_country_map_' . $bid .'" id="fg_lang_country_map_"' . $bid . ' value="' . $default . '" />';
    $out .= '<p class="description">Separate codes by comma. Example: CA,US,MX</p>';
	  echo $out;
  }

  public function fallback_field($options) {
    $default = get_option('fg_lang_country_map_fallback', '/#switch');
    $out = '<select name="fg_lang_country_map_fallback" id="fg_lang_country_map_fallback">';
    $out .= '<option ' . ($default == '/#switch' ? 'selected ' : '') . 'value="/#switch">Language Switcher</option>';
    foreach ($options as $id => $path) {
      $out .= '<option ' . ($default == $path ? 'selected ' : '') . 'value="' . $path . '">' . $path . '</option>';
    }
    $out .= '</select>';
    $out .= '<p class="description">The site a user will be directed to when no country mapping can be found.</p>';
    echo $out;
  }

  public function print_section_info($arg) {
    switch ($arg['id']) {
      case 'names':
        echo 'Settings for the language selector page.';
        break;
      case 'mapping':
        echo 'Enter the country codes that should be mapped to certain sites.';
        break;
      default:
        echo '';
    }
  }

  private function lang_options($keys_only = FALSE) {
    $sites = get_sites();
    $options = array();
    foreach ($sites as $site) {
      // Skip the root site
      if ($site->path != '/') {
          $options[$site->blog_id] = $site->path;
      }
    }
    return $keys_only ? array_keys($options) : $options;
  }

}
