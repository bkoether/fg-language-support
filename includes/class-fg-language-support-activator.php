<?php

/**
 * Fired during plugin activation
 *
 * @link       http://factorsgroup.com
 * @since      1.0.0
 *
 * @package    Fg_Language_Support
 * @subpackage Fg_Language_Support/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Fg_Language_Support
 * @subpackage Fg_Language_Support/includes
 * @author     Ben Koether <bkoether@factorsgroup.com>
 */
class Fg_Language_Support_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
