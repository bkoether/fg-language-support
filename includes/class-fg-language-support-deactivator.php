<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://factorsgroup.com
 * @since      1.0.0
 *
 * @package    Fg_Language_Support
 * @subpackage Fg_Language_Support/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Fg_Language_Support
 * @subpackage Fg_Language_Support/includes
 * @author     Ben Koether <bkoether@factorsgroup.com>
 */
class Fg_Language_Support_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
