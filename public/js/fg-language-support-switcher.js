(function ($) {
  'use strict';


  $(function () {
    // console.log('Switcher Script');
    var baseUrl = window.location.protocol + '//' + window.location.hostname;
    var fallback = baseUrl + '/#switch';

    // If the #switch hash is set we actually don't want to do any redirects.
    if (window.location.hash !== '#switch') {

      // Check if the cookie is set and redirect
      var languageCookie = Cookies.get('fg-language');
      if (languageCookie !== undefined ) {
        var newUrl = baseUrl + languageCookie;
        // console.log(newUrl);
        location.replace(newUrl);
      }
      else {
        // AJAX call to determine where to go.
        $.ajax({
          url: '/wp-admin/admin-ajax.php',//thisBlog.ajax_url,
          data: {
            'action':'geoip_lookup'
          },
          success:function(data) {
            // console.log(data);
            location.replace(baseUrl + data);
          },
          error: function(errorThrown){
            // console.log(errorThrown);
            location.replace(fallback);
          }
        });

      }
    }

  });


})(jQuery);
