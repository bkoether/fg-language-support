(function ($) {
  'use strict';


  $(function () {
    Cookies.set('fg-language', thisBlog.info.path);
  });


  // Overwrite cookie acceptance script
  window.catapultSetCookie = function(cookieName, cookieValue, nDays) {
    var today = new Date();
    var expire = new Date();
    if (nDays==null || nDays==0) nDays=1;
    expire.setTime(today.getTime() + 3600000*24*nDays);
    document.cookie = cookieName+"="+escape(cookieValue)+ ";expires="+expire.toGMTString()+"; path=" + thisBlog.info.path;
  }

})(jQuery);
