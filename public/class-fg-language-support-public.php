<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://factorsgroup.com
 * @since      1.0.0
 *
 * @package    Fg_Language_Support
 * @subpackage Fg_Language_Support/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Fg_Language_Support
 * @subpackage Fg_Language_Support/public
 * @author     Ben Koether <bkoether@factorsgroup.com>
 */
class Fg_Language_Support_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Fg_Language_Support_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Fg_Language_Support_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/fg-language-support-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Fg_Language_Support_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Fg_Language_Support_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */


		wp_enqueue_script( 'cookie_js', plugin_dir_url( __FILE__ ) . 'js/js.cookie.js', array( 'jquery' ), $this->version, false );
		if (get_current_blog_id() == 1) {
      wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/fg-language-support-switcher.js', array( 'jquery' ), $this->version, false );
    }
    else {
      wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/fg-language-support-public.js', array( 'jquery' ), $this->version, true );
    }

		$bloginfo = array(
		  'info' => get_blog_details(),
      'ajax_url' => admin_url( 'admin-ajax.php' )
    );
    wp_localize_script($this->plugin_name, 'thisBlog', $bloginfo );
	}

	public function geoIpLookup () {
	  $url = get_option('fg_lang_country_map_fallback', '/#switch');
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
      $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
      $ip = $_SERVER['REMOTE_ADDR'];
    }

    $args = array(
      'headers' => array(
        'Authorization' => 'Basic ' . base64_encode( '95225:UyhsshotPWh0')
      )
    );

    $response = wp_remote_get('https://geoip.maxmind.com/geoip/v2.1/country/' . $ip, $args);

    if( is_array($response) ) {
      $body = json_decode($response['body']); // use the content
      $sites = get_sites();
        foreach ($sites as $site) {
          $mapping = get_option('fg_lang_country_map_' . $site->blog_id, '');
          if (!empty($mapping)) {
            $codes = explode(',', $mapping);
            if (in_array($body->country->iso_code, $codes)) {
              echo $site->path;
              die();
            }
          }
        }
    }

    echo $url;
    die();

  }

}
